### A Pluto.jl notebook ###
# v0.14.7

using Markdown
using InteractiveUtils

# ╔═╡ 73fc324b-b7f3-409a-8dbb-3f468c9d8799
using Pkg

# ╔═╡ e226dfce-ed75-4438-8607-e5353f8ee095
Pkg.activate("Project.toml")

# ╔═╡ ea15551e-74f0-4798-8203-789a034e1086
using PlutoUI

# ╔═╡ 3b143fbc-12c0-4410-a755-7215e7c3c3ab
board = [[" ", " ", " "],
         [" ", " ", " "],
         [" ", " ", " "]]

# ╔═╡ 033ffa9a-d4ba-4827-92a2-f99f62ea2758
p1 = "x"

# ╔═╡ 911ec2ab-9c40-4cf2-bc6c-51d8997b4ddf
p2 = "o" 

# ╔═╡ 104ff88e-ee1d-4736-ac1a-29eeca0351f5
p1_win = -1

# ╔═╡ fe2aee1e-8ecf-4ad8-aa62-0c2b406c2b6e
tie = 0

# ╔═╡ f56713dd-d230-4c08-b619-fb526eaaddb8
p2_win = 1

# ╔═╡ 6a0e8baf-ab57-4804-90c2-95e596487ecc
function print_board(board)
    println("    1   2   3")
    println(" 1  ", join(board[1], " | "))
    println("   ———┼———┼———")
    println(" 2  ", join(board[2], " | "))
    println("   ———┼———┼———")
    println(" 3  ", join(board[3], " | "))
end

# ╔═╡ 7e17c1f7-591a-4106-9f0e-d012cc59422f
function check_available(board)
    available_cells = []
    for i in 1:3, j in 1:3
        if board[i][j] == " "
            push!(available_cells, (i, j))
        end
    end
    return available_cells
end


# ╔═╡ f02a6824-d436-424d-a71e-28ea31455502
md"# Check Win Function"

# ╔═╡ 6123141b-2041-41dd-99e5-181cdff5bd39
function Checkwin(board)

    # The Rows
    for i in 1:3
        if (board[i][1] == board[i][2] == board[i][3]) && (board[i][1] in [p1, p2])
            if (board[i][1] == p1)
                return p1_win
            end
            return p2_win
        end
    end

    # The Columns
    for i in 1:3
        if (board[1][i] == board[2][i] == board[3][i]) && (board[1][i] in [p1, p2])
            if (board[1][i] == p1)
                return p1_win
            end
            return p2_win
        end
    end

    # The Principal diagonal
    if (board[1][1] == board[2][2] == board[3][3]) && (board[1][1] in [p1, p2])
        if (board[1][1] == p1)
            return p1_win
        end
        return p2_win
    end

    # Other diagonal
    if (board[1][3] == board[2][2] == board[3][1]) && (board[1][3] in [p1, p2])
        if (board[1][3] == p1)
            return p1_win
        end
        return p2_win
    end

    # When there is a Tie
    if length(check_available(board)) == 0
        return tie
    end

 
    return nothing
end

# ╔═╡ 893b8770-0a4f-46b5-93cd-db0f8b646a57
md"# Make Move Function"

# ╔═╡ 00b9fd8d-52fe-4994-ad77-d385951b46e7
function make_move!(board, player, (x, y))
    board[x][y] = player
end

# ╔═╡ 063a23ca-b479-453a-bf3d-58adb0217450
md"# Best Move Fucntion"

# ╔═╡ f15f8a91-ebf6-459c-a98c-f78216510b35
md"# Worst Move Function"

# ╔═╡ 39edac54-2ca4-4735-9a01-6dc3b91a1a91
md"# AI Move Funtion"

# ╔═╡ 7ca25980-6d29-45c3-821d-e93c34e08bbb
md"# Minimax Function"

# ╔═╡ b03bc9c8-0c6e-46cb-afe1-fb78ab163fee
function minimax(board, depth, is_maximising)
    result = Checkwin(board)

    if result !== nothing
        return result
    elseif is_maximising

        best_score = -Inf

        for i in 1:3, j in 1:3
            if board[i][j] == " "

                board[i][j] = p2

                score = minimax(board, depth + 1, false)
                
                board[i][j] = " "

                best_score = max(score, best_score)
            end
        end

        return best_score        
    else        
        best_score = Inf

        for i in 1:3, j in 1:3
            if board[i][j] == " "

                board[i][j] = p1

                score = minimax(board, depth + 1, true)
                
                board[i][j] = " "

                best_score = min(score, best_score)
            end
        end

        return best_score   
    end

end

# ╔═╡ a2f2c795-eba7-4640-b986-4060d71f7b1e
function best_move(board)
    best_score = -Inf

    best_pos = (0, 0)

    for i in 1:3, j in 1:3
        if board[i][j] == " "

            board[i][j] = p2
            
            score = minimax(board, 0, false)

            
            board[i][j] = " "

            if score > best_score
                best_score = score
                best_pos = (i, j)
            end
        end
    end
    return best_pos
end

# ╔═╡ 4e596937-ed07-4e03-9c51-153529aceb48
function worst_move(board)
    worst_score = Inf

    worst_pos = (0, 0)

    for i in 1:3, j in 1:3
        if board[i][j] == " "

            board[i][j] = p2
            
            score = minimax(board, 0, true)

            
            board[i][j] = " "

            if score < worst_score
                worst_score = score
                worst_pos = (i, j)
            end
        end
    end
    return worst_pos
end


# ╔═╡ 05d5bb34-e5ea-4f8e-b3a0-b879bca7956d

function AI_move(board, difficulty)
    if difficulty == "easy easy"
        make_move!(board, p2, worst_move(board))
    elseif difficulty == "Easy"
        pos = rand(check_available(board))
        make_move!(board, p2, pos)
    elseif difficulty == "Medium"
        if rand() < 0.3
            make_move!(board, p2, best_move(board))
        else            
            pos = rand(check_available(board))
            make_move!(board, p2, pos)
        end
    elseif difficulty == "Hard"
        if rand() < 0.7
            make_move!(board, p2, best_move(board))
        else            
            pos = rand(check_available(board))
            make_move!(board, p2, pos)
        end
    elseif difficulty == "Impossible"
        make_move!(board, p2, best_move(board))
    end
    
end

# ╔═╡ eeabf0d3-ef89-4a43-afd1-7a32f88713a8
md"# Move Input Function"

# ╔═╡ 0a1d2f4e-7940-45c5-98fb-24ef656ae915
function move_input(board, prompt, params)

    print(prompt)
    txt = split(readline())

    if txt == ["exit"]
        exit()
    end

    if length(txt) == params && txt[1] in string.(collect(1:3)) && txt[2] in string.(collect(1:3)) && (board[parse(Int, txt[1])][parse(Int, txt[2])] == " ")
        return txt
    else
        println("Please enter a valid command (See README)")
        move_input(board, prompt, params)
    end
end


# ╔═╡ c66631dd-c606-47a0-9b28-0f58e018d156
md"# Generalized Input Function"

# ╔═╡ 46882c90-e0ff-4187-9388-895b96985384
function generalised_input(prompt, same_line, choices, params)
    same_line ? print(prompt) : println(prompt)

    txt = split(lowercase(readline()))

    if txt == ["exit"]
        exit()
    end

    if length(txt) == params && txt[1] in choices
        return txt
    else
        println("Please enter a valid command (See README)")
        generalised_input(prompt, same_line, choices, params)
    end
end

# ╔═╡ 9b9f8e18-6fc9-44f7-96bf-abac1facff5a
md"# Single Player Funtion"

# ╔═╡ f9562703-518f-4c07-9833-af1d859c78a6
function singleplayer(board, (p1, p2), (p1_win, p2_win, tie))
    diff_levels = """
    Choose a difficulty level:
    - `Easy easy` - The AI will always make the worst move
    - `Easy` - The AI will play randomly
    - `Medium` -The AI will make some mistakes
    - `Hard` -The AI will make very few mistakes
    - `Impossible` - The AI will always make the best move. It will be Impossible to win.
    """

    println(diff_levels)
    difficulty = generalised_input("Difficulty: ", true, ["easy easy", "Easy", "Medium", "Hard", "Impossible"], 1)[1]

    println("Human is X, AI is O. Human goes first.")
    print_board(board)

    while true

        txt = move_input(board, "Enter position of cell (row and column seperated by space): ", 2)

        make_move!(board, p1, (parse(Int, txt[1]), parse(Int, txt[2])))

        if Checkwin(board) == p1_win
            println("Human wins!")
            break
        elseif Checkwin(board) == p2_win
            println("AI wins.")
            break
        elseif Checkwin(board) == tie
            println("Tie!")
            break
        end

        AI_move(board, difficulty)
        print_board(board)

        if Checkwin(board) == p1_win
            println("Human wins!")
            break
        elseif Checkwin(board) == p2_win
            println("AI wins.")
            break
        elseif Checkwin(board) == tie
            println("Tie!")
            break
        end
    end
end

# ╔═╡ 3535701f-710e-4745-bbd7-499286a3d3c5
md"# Multiplayer Function"

# ╔═╡ 3367414c-a768-4dd2-a412-508cba9429ed
function multiplayer(board, (p1, p2), (p1_win, p2_win, tie))
    println("Player 1 is X, Player 2 is O. Player 1 goes first.")
    print_board(board)

    while true

        txt = move_input(board, "Enter position of cell (row and column seperated by space): ", 2)

        make_move!(board, p1, (parse(Int, txt[1]), parse(Int, txt[2])))
        print_board(board)

        if Checkwin(board) == p1_win
            println("Player 1 Wins!")
            break
        elseif Checkwin(board) == p2_win
            println("Player 2 Wins!")
            break
        elseif Checkwin(board) == tie
            println("Tie!")
            break
        end

        txt = move_input(board, "Enter position of cell (row and column seperated by space): ", 2)

        make_move!(board, p2, (parse(Int, txt[1]), parse(Int, txt[2])))

        print_board(board)


        if Checkwin(board) == p1_win
            println("Player 1 Wins!")
            break
        elseif Checkwin(board) == p2_win
            println("Player 2 Wins!")
            break
        elseif Checkwin(board) == tie
            println("Tie!")
            break
        end
    end
end


# ╔═╡ 8a001e22-d8ab-4af2-9590-10a6ea150c9b
md"# Play Again Function"

# ╔═╡ 719bf09e-df70-4aad-85dd-0772bc1dd99e
function play_again()
    replay = generalised_input("Do you wish to Play again? (y/n): ", true, ["y", "n"], 1)[1]
    if replay == "y"
        run(`julia $PROGRAM_FILE`)
        exit()
    else
        exit()
    end
end

# ╔═╡ 301d5ef1-d072-4fcd-9600-dfa3bb0c8478
mode = generalised_input("Welcome to the TicTacToe Game! Would you like to play as a singleplayer (1) or would you like to play as multiplayers (2)?", false, ["1", "2"], 1)[1]

# ╔═╡ 139b6874-0f95-44ac-b722-fbf130ddfa05
if mode == "1"
    singleplayer(board, (p1, p2), (p1_win, p2_win, tie))
else
    multiplayer(board, (p1, p2), (p1_win, p2_win, tie))
end

# ╔═╡ 8d47abc6-afba-4e26-b9a5-e13e817649a5
play_again()

# ╔═╡ Cell order:
# ╠═73fc324b-b7f3-409a-8dbb-3f468c9d8799
# ╠═e226dfce-ed75-4438-8607-e5353f8ee095
# ╠═ea15551e-74f0-4798-8203-789a034e1086
# ╠═3b143fbc-12c0-4410-a755-7215e7c3c3ab
# ╠═033ffa9a-d4ba-4827-92a2-f99f62ea2758
# ╠═911ec2ab-9c40-4cf2-bc6c-51d8997b4ddf
# ╠═104ff88e-ee1d-4736-ac1a-29eeca0351f5
# ╠═fe2aee1e-8ecf-4ad8-aa62-0c2b406c2b6e
# ╠═f56713dd-d230-4c08-b619-fb526eaaddb8
# ╠═6a0e8baf-ab57-4804-90c2-95e596487ecc
# ╠═7e17c1f7-591a-4106-9f0e-d012cc59422f
# ╟─f02a6824-d436-424d-a71e-28ea31455502
# ╠═6123141b-2041-41dd-99e5-181cdff5bd39
# ╟─893b8770-0a4f-46b5-93cd-db0f8b646a57
# ╠═00b9fd8d-52fe-4994-ad77-d385951b46e7
# ╟─063a23ca-b479-453a-bf3d-58adb0217450
# ╠═a2f2c795-eba7-4640-b986-4060d71f7b1e
# ╟─f15f8a91-ebf6-459c-a98c-f78216510b35
# ╠═4e596937-ed07-4e03-9c51-153529aceb48
# ╟─39edac54-2ca4-4735-9a01-6dc3b91a1a91
# ╠═05d5bb34-e5ea-4f8e-b3a0-b879bca7956d
# ╟─7ca25980-6d29-45c3-821d-e93c34e08bbb
# ╠═b03bc9c8-0c6e-46cb-afe1-fb78ab163fee
# ╟─eeabf0d3-ef89-4a43-afd1-7a32f88713a8
# ╠═0a1d2f4e-7940-45c5-98fb-24ef656ae915
# ╟─c66631dd-c606-47a0-9b28-0f58e018d156
# ╠═46882c90-e0ff-4187-9388-895b96985384
# ╟─9b9f8e18-6fc9-44f7-96bf-abac1facff5a
# ╠═f9562703-518f-4c07-9833-af1d859c78a6
# ╟─3535701f-710e-4745-bbd7-499286a3d3c5
# ╠═3367414c-a768-4dd2-a412-508cba9429ed
# ╟─8a001e22-d8ab-4af2-9590-10a6ea150c9b
# ╠═719bf09e-df70-4aad-85dd-0772bc1dd99e
# ╠═301d5ef1-d072-4fcd-9600-dfa3bb0c8478
# ╠═139b6874-0f95-44ac-b722-fbf130ddfa05
# ╠═8d47abc6-afba-4e26-b9a5-e13e817649a5
