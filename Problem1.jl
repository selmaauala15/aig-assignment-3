### A Pluto.jl notebook ###
# v0.14.7

using Markdown
using InteractiveUtils

# ╔═╡ 589a3a34-eb33-47e9-a61d-8590e0dc71ac
using Pkg

# ╔═╡ 817c780a-6d13-457c-9adf-13c09833180c
Pkg.activate("Project.toml")

# ╔═╡ a48bf4ab-7388-4014-8d54-1166e55c987e
using PlutoUI

# ╔═╡ 090fef15-c654-4692-a401-b6cb0cd160d8
using InstantiateFromURL

# ╔═╡ f90074e5-c243-4ba0-ab58-e1240d4c84ee
using LinearAlgebra, Statistics

# ╔═╡ acdbc880-c237-4ccb-aa2e-d1506ddc5724
using Distributions, Plots, Printf, QuantEcon, Random

# ╔═╡ 874feee9-e5da-4559-8551-9d7709b5de4c
github_project("QuantEcon/quantecon-notebooks-julia", version = "0.8.0")

# ╔═╡ b7297c5e-161c-4940-9918-467b1005a395
gr(fmt = :png);

# ╔═╡ 9615bab8-fda6-4296-8842-0f5f1261e4f8
md"# 3 Descrete States"

# ╔═╡ d4c1ed43-184f-4561-95a0-3441134cd35d
d = Categorical([0.5, 0.3, 0.2]) # 3 discrete states

# ╔═╡ 2702a38a-92c6-4941-b879-9708bbed6c30
@show rand(d, 5)

# ╔═╡ 6cb397ba-4579-424a-93ee-4f79f5a27d98
@show supertype(typeof(d))

# ╔═╡ c2f73ad1-c8d7-48ef-b69d-6066cd0e566c
md"# The probability to be in state 1"

# ╔═╡ e4be05a1-4a5c-4cfd-9596-968d82332803
@show pdf(d, 1) 

# ╔═╡ 1eca8a70-8ce8-414a-a0b3-d0c8ab239169
@show support(d)

# ╔═╡ 10daba14-eae0-403e-a9a5-c993b4eb5a31
md"# Broadcast the pdf over the whole support"

# ╔═╡ 5b9a707b-01aa-415b-9c12-8fb075626d8d
@show pdf.(d, support(d)); 

# ╔═╡ 78f95a94-7243-4985-988e-28b4a0d8c7c3
P = [0.4 0.6; 0.2 0.8]

# ╔═╡ 67d55095-667d-4a9f-be71-cc319729d126
md" # .== Broadcasts test for equality."

# ╔═╡ b88e9bc6-1ebb-4c2b-aa25-157a49a8f33e
0.24773

# ╔═╡ d857d7de-4cab-4f14-b055-2e200c5f4000
mc = MarkovChain(P, ["employed", "unemployed"])

# ╔═╡ 04417dc1-efb3-41a2-9f3c-edac14e26158
X = simulate(mc, 100_000);

# ╔═╡ 1f59e64e-a0a0-4ac5-aa8e-93ebe8d54f31
μ_1 = count(X .== 1)/length(X) 

# ╔═╡ 6716893a-6b69-4db2-bde6-ec860d3736f2
μ_2 = count(X .== 1)/length(X) 

# ╔═╡ 58abe897-2bfc-4771-ac55-2be3deec9a7d
md"# Start at State 1"

# ╔═╡ d872491b-35e9-4fa7-a73f-77d7803cf5d9
simulate(mc, 4, init = 1)

# ╔═╡ 7a6c2675-575f-4172-92e6-fc5f2446850d
md"# Start at state 2"

# ╔═╡ d107ed6e-97bb-45d4-8bba-0ba5b49f47cc
simulate(mc, 4, init = 2) 

# ╔═╡ 5060d4b3-2a69-4ba3-970c-4fb578da0a36
md"# Start with a randomly chosen initial condition"

# ╔═╡ 21328b2f-df58-473e-a112-d6896bd9ea24
simulate(mc, 4) 

# ╔═╡ 504ee37a-65df-4dca-a7f5-08a1c146b746
simulate_indices(mc, 4)

# ╔═╡ Cell order:
# ╠═589a3a34-eb33-47e9-a61d-8590e0dc71ac
# ╠═817c780a-6d13-457c-9adf-13c09833180c
# ╠═a48bf4ab-7388-4014-8d54-1166e55c987e
# ╠═090fef15-c654-4692-a401-b6cb0cd160d8
# ╠═874feee9-e5da-4559-8551-9d7709b5de4c
# ╠═f90074e5-c243-4ba0-ab58-e1240d4c84ee
# ╠═acdbc880-c237-4ccb-aa2e-d1506ddc5724
# ╠═b7297c5e-161c-4940-9918-467b1005a395
# ╟─9615bab8-fda6-4296-8842-0f5f1261e4f8
# ╠═d4c1ed43-184f-4561-95a0-3441134cd35d
# ╠═2702a38a-92c6-4941-b879-9708bbed6c30
# ╠═6cb397ba-4579-424a-93ee-4f79f5a27d98
# ╟─c2f73ad1-c8d7-48ef-b69d-6066cd0e566c
# ╠═e4be05a1-4a5c-4cfd-9596-968d82332803
# ╠═1eca8a70-8ce8-414a-a0b3-d0c8ab239169
# ╟─10daba14-eae0-403e-a9a5-c993b4eb5a31
# ╠═5b9a707b-01aa-415b-9c12-8fb075626d8d
# ╠═78f95a94-7243-4985-988e-28b4a0d8c7c3
# ╠═04417dc1-efb3-41a2-9f3c-edac14e26158
# ╠═67d55095-667d-4a9f-be71-cc319729d126
# ╠═1f59e64e-a0a0-4ac5-aa8e-93ebe8d54f31
# ╠═b88e9bc6-1ebb-4c2b-aa25-157a49a8f33e
# ╠═d857d7de-4cab-4f14-b055-2e200c5f4000
# ╠═6716893a-6b69-4db2-bde6-ec860d3736f2
# ╟─58abe897-2bfc-4771-ac55-2be3deec9a7d
# ╠═d872491b-35e9-4fa7-a73f-77d7803cf5d9
# ╟─7a6c2675-575f-4172-92e6-fc5f2446850d
# ╠═d107ed6e-97bb-45d4-8bba-0ba5b49f47cc
# ╟─5060d4b3-2a69-4ba3-970c-4fb578da0a36
# ╠═21328b2f-df58-473e-a112-d6896bd9ea24
# ╠═504ee37a-65df-4dca-a7f5-08a1c146b746
